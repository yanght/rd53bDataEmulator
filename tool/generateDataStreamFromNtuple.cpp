#include <iostream>
#include <fstream>
#include <getopt.h>
#include <memory>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"

#include "RD53BEncoding/ChipMap_RD53B.h"
#include "RD53BEncoding/RD53BEncodingTool.h"

using namespace std;

struct header
{
  uint32_t l1id : 16;
  uint32_t hits : 16;
};

struct hit
{
  uint32_t x : 9;
  uint32_t y : 9;
  uint32_t q : 14;
};

int convertChargeToToT(int charge)
{
  // For now use ITkPix-V1.1 convention
  return 7 + 1;
}

pair<int, int> convertSensorToPixAddress(int px, int py, bool inner = false)
{
  if (inner)
  {
    int x = (py % 2 == 0) ? (2 * px + 1) : (2 * px);
    int y = (py / 2);
    return make_pair(x, y);
  }
  else
    return make_pair(px, py);
}

struct option longopts[] = {
    {"input", required_argument, NULL, 'i'},
    {"nEvtPerStream", required_argument, NULL, 'n'},
    {"plain", required_argument, NULL, 'p'},
    {"dir", required_argument, NULL, 'd'},
    {"help", no_argument, NULL, 'h'},
    {0, 0, 0, 0}};

void printHelp(std::string exe)
{
  cout << "Usage: " << exe << " [options]" << endl;
  cout << "Allowed options:" << endl;
  cout << " -i [ --input ] arg                 Input file name (default \"\")" << endl;
  cout << " -n [ --nEvtPerStream ] arg         Number of events in each stream (default 1)" << endl;
  cout << " -p [ --plainHitMap ] arg           Whether to generate plain hit map (default false)" << endl;
  cout << " -d [ --dir ] arg                   Output dir (default pwd)" << endl;
  cout << " -h [ --help ]                      Produce help message" << endl;
}

int main(int argc, char **argv)
{
  int N_E = 1;
  bool plainHitMap = false;
  std::string outputDir = ".";
  TString filename = "";
  int oc;
  while ((oc = getopt_long(argc, argv, "i:n:p:d:h", longopts, NULL)) != -1)
  {
    switch (oc)
    {
    case 'i':
      filename = optarg;
      cout << "Set input file name to " << filename << endl;
      break;      
    case 'n':
      N_E = atoi(optarg);
      cout << "Set number of events per stream to " << N_E << endl;
      break;
    case 'p':
      plainHitMap = atoi(optarg);
      cout << "Set generate plain hit map to " << plainHitMap << endl;
      break;
    case 'd':
      outputDir = optarg;
      cout << "Set output dir to " << outputDir << endl;
      break;
    case 'h':
      printHelp(argv[0]);
      return 0;
    case ':': /* missing option argument */
      fprintf(stderr, "%s: option `-%c' requires an argument\n",
              argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    case '?':
    default:
      fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
              argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    }
  }
  std::cout << "Opening file:" << filename << std::endl;
  TFile *file = TFile::Open(filename);
  TTree *tree = (TTree *)file->Get("t");

  const Int_t maxHits = 1024;
  Int_t hits;
  Int_t px[maxHits];
  Int_t py[maxHits];
  Int_t pcharge[maxHits];

  tree->SetBranchAddress("ndigis_on_det", &hits);
  tree->SetBranchAddress("px", px);
  tree->SetBranchAddress("py", py);
  tree->SetBranchAddress("pcharge", pcharge);

  std::ofstream outfile("out.bin", std::ofstream::out | std::ofstream::binary);

  long int number_of_events = 0;
  long int number_of_hits = 0;

  // Set up data encoder
  RD53BEncodingTool *tool = new RD53BEncodingTool();
  tool->setEventsPerStream(N_E);
  tool->setPlainHitMap(plainHitMap);
  tool->setOutputDir(outputDir);
  tool->initialize();

  unique_ptr<ChipMap_RD53B> map;
  for (unsigned i = 0; i < tree->GetEntries(); i++)
  {
    tree->GetEntry(i);
    std::cout << "Event #" << i << " with " << hits << " hits." << std::endl;
    struct header head;
    head.l1id = i;
    head.hits = hits;
    outfile.write((char *)&head, sizeof(head));
    number_of_events++;
    map.reset(new ChipMap_RD53B(400, 384, 8, 2));
    for (unsigned n = 0; n < hits; n++)
    {
      std::cout << px[n] << " " << py[n] << " " << pcharge[n] << std::endl;
      struct hit h;
      h.x = px[n];
      h.y = py[n];
      h.q = (pcharge[n] / 1000) & 0x3FFF;
      outfile.write((char *)&h, sizeof(head));
      number_of_hits++;

      // Fill chip map
      auto pixAddr = convertSensorToPixAddress(h.y, h.x, false);
      int ToT = convertChargeToToT(h.q);
      map->fillChipMap(pixAddr.first, pixAddr.second, ToT);
    }
    tool->createStream(*map);
  }
  outfile.close();
  tool->saveDataStream();

  std::cout << "Number of events: " << number_of_events << std::endl;
  std::cout << "Number of hits: " << number_of_hits << std::endl;
}
