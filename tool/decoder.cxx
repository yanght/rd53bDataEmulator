#include <iostream>
#include <fstream>
#include <getopt.h>

#include "RD53BDecoding/Rd53bDataProcessor.h"

using namespace std;
using namespace RD53BDecoding;

struct option longopts[] = {
			    { "nStream", required_argument, NULL, 'n'},
			    { "inputStreamFileName", required_argument, NULL, 's'},
			    { "inputXCheckFileName", required_argument, NULL, 'x'},
			    { "debug", required_argument, NULL, 'd'},
			    { "repeat", required_argument, NULL, 'r'},
			    { "outputFileName", required_argument, NULL, 'o'},
			    { "help", no_argument, NULL, 'h'},
			    {0, 0, 0, 0}
};

void printHelp(std::string exe){
  std::cout<<"Usage: "<<exe<<" [options]"<<std::endl;
  std::cout<<"Allowed options:"<<std::endl;
  std::cout<<" -n [ --nStream ] arg               Number of streams to read (default -1)"<<std::endl;
  std::cout<<" -s [ --inputStreamFileName ] arg   Input stream file name (default \"rd53b_stream.txt\")"<<std::endl;
  std::cout<<" -x [ --inputXCheckFileName ] arg   Input x-check file name (default \"rd53b_chip.txt\")"<<std::endl;
  std::cout<<" -d [ --debug ] arg                 Debug flag (default false)"<<std::endl;
  std::cout<<" -r [ --repeat ] arg                Repeat the decoding for n times (default 1)"<<std::endl;
  std::cout<<" -o [ --outputFileName ] arg        Output file name (default summary.txt)"<<std::endl;
  std::cout<<" -h [ --help ]                      Produce help message"<<std::endl;
}

int main(int argc, char** argv){
  if( argc < 1 ){
    printf("Usage: %s <input stream file> <input cross-check file> <debug> \n", argv[0]);
    return 0;
  }
  std::string inputStreamFileName = "rd53b_stream.txt";
  std::string inputXCheckFileName = "rd53b_chip.txt";
  std::string outputFileName = "summary.txt";
  int nStream = -1;
  int repeat = 1;
  int debug = 0;
  
  int oc;
  while ((oc=getopt_long(argc, argv, "n:s:x:d:r:o:h", longopts, NULL)) != -1){
    switch (oc) {
    case 'n':
      nStream = atoi(optarg);
      std::cout << "Set number of streams to process to " << nStream << std::endl;
      break;
    case 's':
      inputStreamFileName = optarg;
      std::cout << "Set input stream file name to " << inputStreamFileName << std::endl;
      break;
    case 'x':
      inputXCheckFileName = optarg;
      std::cout << "Set input x-check file name to " << inputXCheckFileName << std::endl;
      break;
    case 'd':
      debug = atoi(optarg);
      std::cout << "Set debug level to " << debug << std::endl;
      break;
    case 'r':
      repeat = atoi(optarg);
      std::cout << "Set repeat times to " << repeat << std::endl;
      break;
    case 'o':
      outputFileName = optarg;
      std::cout << "Set output file name to " << outputFileName << std::endl;
      break;
    case 'h':
      printHelp(argv[0]);
      return 0;
    case ':':   /* missing option argument */
      fprintf(stderr, "%s: option `-%c' requires an argument\n",
	      argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    case '?':
    default:
      fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
	      argv[0], optopt);
      printHelp(argv[0]);
      return 0;
    }
  }
  ofstream fout(outputFileName);
  
  Rd53bDataProcessor *processor = new Rd53bDataProcessor(400, 384);
  processor->setDebug(debug);
  
  int wordCount = processor->readInData(inputStreamFileName, inputXCheckFileName, nStream);

  for(int i = 0; i < repeat; i++){
    if(i !=0) processor->reset(true);
    double durition = processor->process();
    fout<<durition<<" ";
  }
  fout<<endl;
  
  int nHits = processor->finalize();
  fout << wordCount << " " << nHits << endl;

  fout.close();

}
