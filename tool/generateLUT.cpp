#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>

#define HEXF(x, y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << (y) << std::dec

void fillLUT_BinaryTreeToPlainHMap(std::string outputFileName){
  std::cout << "Filling lookup table translating 4th layer binary tree to 16-bit hitmap...";
  std::ofstream fout(outputFileName);
  fout << "#ifndef LUT_BinaryTreeToPlainHMap_H" << std::endl;
  fout << "#define LUT_BinaryTreeToPlainHMap_H" << std::endl;
  fout << "namespace RD53BDecoding{" << std::endl;
  fout << "\tstatic constexpr uint16_t _LUT_BinaryTree_To_PlainHMap[65536] = {" << std::endl;
  for(int bt = 0; bt <= 0xFFFF; bt++){
    fout << HEXF(4, (bt & 0xF000) | ((bt & 0x0F00) >> 4) | ((bt & 0x0F0) << 4) | (bt & 0x000F)) << ", ";
  }
  fout << "\t};" << std::endl;
  fout << "}" << std::endl;
  fout << "#endif" << std::endl;
  fout.close();
  std::cout << "Done" << std::endl;
}

void fillLUT_PlainHMapToColRow(std::string outputFileName){
  std::cout << "Filling lookup table translating 16-bit hitmap to column/row numbers...";
  std::ofstream fout(outputFileName);
  std::stringstream ss;
  
  fout << "#ifndef LUT_PlainHMapToColRow_H" << std::endl;
  fout << "#define LUT_PlainHMapToColRow_H" << std::endl;
  fout << "namespace RD53BDecoding{" << std::endl;
  fout << "\tstatic constexpr uint8_t _LUT_PlainHMap_To_ColRow[65536][16] = {" << std::endl;
  for(unsigned hmap = 0; hmap <= 0xFFFF; hmap++){
    int counter = 0;
    fout << "\t\t{";
    for(int ib = 15; ib >= 0; ib--){
      if(((hmap >> ib) & 0x1) == 0) continue;
      fout << HEXF(2, ((ib % 8) << 4) | (ib/8)) << ", ";
      counter++;
    }
    fout << "}," << std::endl;
    ss << counter << ", ";
  }
  fout << "\t};" << std::endl;
  
  fout << "\tstatic constexpr uint8_t _LUT_PlainHMap_To_ColRow_ArrSize[65536] = {" << ss.str() << "};" << std::endl;
  fout << "}" << std::endl;
  fout << "#endif" << std::endl;
  fout.close();
  std::cout << "Done" << std::endl;
}

int main(int argc, char** argv){
  fillLUT_BinaryTreeToPlainHMap("../RD53BDecoding/RD53BDecoding/LUT_BinaryTreeToPlainHMap.h");
  fillLUT_PlainHMapToColRow("../RD53BDecoding/RD53BDecoding/LUT_PlainHMapToColRow.h");
}
