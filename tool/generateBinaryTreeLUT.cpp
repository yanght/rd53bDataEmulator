#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>
#include <iomanip>

using namespace std;

#define HEXF(x, y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << (y) << std::dec

static constexpr uint8_t _LUT_BinaryTreeMaskSize[3] = {4, 2, 1};
static constexpr uint8_t _LUT_BinaryTreeMask[3][8] = {
							      // {0x0F, 0x00, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00},
							      // {0xF3, 0x00, 0xFC, 0x00, 0x3F, 0x00, 0xCF, 0x00},
							      // {0xFD, 0xFE, 0xF7, 0xFB, 0xDF, 0xEF, 0x7F, 0xBF},
						      {0xF0, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00},
						      {0xCF, 0x00, 0x3F, 0x00, 0xFC, 0x00, 0xF3, 0x00},
						      {0xBF, 0x7F, 0xEF, 0xDF, 0xFB, 0xF7, 0xFE, 0xFD},
};

const int BINARYTREE_DEPTH = 4;

struct hitmap{
  uint64_t stream;
  int bitIdx;
  int length;
};

uint8_t retrieve(hitmap &input, const unsigned length){
  uint64_t mask = (0xFFFFFFFFFFFFFFFF >> (64 - input.length)) >> input.bitIdx;
  input.bitIdx += length;
  return ((input.stream & mask) >> (input.length - input.bitIdx));
}

void rollBack(hitmap &input, const unsigned length){
  // cout<<__PRETTY_FUNCTION__<<endl;
  input.bitIdx -= length;
}

uint8_t getBitPair(hitmap &input){
  if(input.bitIdx > input.length - 2) return 0;
  // if(_debug >= 3) std::cout << __PRETTY_FUNCTION__ << std::endl;
  uint8_t bitpair = retrieve(input, 2);
  // There is only 11 and 10, and 01 is replaced with 0
  // Whenever 00 or 01 show up, replace the first 0 with 01, and move 1-bit back
  if(bitpair == 0x0 || bitpair == 0x1){
    bitpair = 0x1; // 01
    rollBack(input, 1); // Moving bit index back by 1
  }
  return bitpair;
}

std::pair<uint16_t, uint16_t> decodeRowMap(hitmap &input){
  uint16_t lowestLayer = 0XFF; // Start from all 1. Pixels not fired will be masked by 0
  uint16_t nRead[BINARYTREE_DEPTH] = {1, 0, 0, 0};
  uint16_t nShift[BINARYTREE_DEPTH][16]={{0}};

  int bitIdx_init = input.bitIdx;
  
  for(uint8_t lv = 0; lv < BINARYTREE_DEPTH - 1; lv++){ // For each row there 
    for(uint8_t ir = 0; ir < nRead[lv]; ir++){
      uint8_t bitpair = getBitPair(input);
      switch(bitpair){
      case 0x1: // 01
	lowestLayer &= _LUT_BinaryTreeMask[lv][nShift[lv][ir]];
	nShift[lv + 1][nRead[lv + 1]++] = nShift[lv][ir];
	break;
      case 0x2: // 10
	lowestLayer &= _LUT_BinaryTreeMask[lv][nShift[lv][ir] + _LUT_BinaryTreeMaskSize[lv]];
	nShift[lv + 1][nRead[lv + 1]++] = nShift[lv][ir] + _LUT_BinaryTreeMaskSize[lv];
	break;
      case 0x3: // 11
	lowestLayer &= (_LUT_BinaryTreeMask[lv][nShift[lv][ir]] | _LUT_BinaryTreeMask[lv][nShift[lv][ir] + _LUT_BinaryTreeMaskSize[lv]]);
	nShift[lv + 1][nRead[lv + 1]++] = nShift[lv][ir] + _LUT_BinaryTreeMaskSize[lv];
	nShift[lv + 1][nRead[lv + 1]++] = nShift[lv][ir];
	break;
      case 0x0:			// Not enough bits
      	return std::make_pair(0, input.length - bitIdx_init);
      }
    }
  }
  return std::make_pair(lowestLayer, input.length - input.bitIdx);
}

void prepareRowLUT(){
  const int length = 14;	// Decode each row, which requires at most 14 bits
  const int nmap = pow(2, length);
  std::stringstream outputFileName;
  outputFileName << "../RD53BDecoding/RD53BDecoding/LUT_BinaryTreeRowHMap.h";
  std::ofstream fout(outputFileName.str());
  fout << "#ifndef LUT_BinaryTreeRowHMap_H" << std::endl;
  fout << "#define LUT_BinaryTreeRowHMap_H" << std::endl;
  fout << "namespace RD53BDecoding{" << std::endl;
  fout << "\tstatic constexpr uint16_t _LUT_BinaryTreeRowHMap[" << nmap << "] = {" << std::endl;
  for(uint64_t imap = 0; imap < nmap; imap++){
    hitmap input;
    input.stream = imap;
    input.bitIdx = 0;
    input.length = length;		// First iteration: read 8-bit
    auto result = decodeRowMap(input);
    uint16_t summary = (result.second << 8) | result.first;
    fout << HEXF(4, summary) << ", ";
  }
  fout << "};" << std::endl;
  fout << "}" << std::endl;
  fout << "#endif" << std::endl;
  fout.close();
}

void prepareHMapLUT(){
  const int length = 16;	// Decode each row, which requires at most 14 bits
  const int nmap = pow(2, length);
  std::stringstream outputFileName;
  outputFileName << "../RD53BDecoding/RD53BDecoding/LUT_BinaryTreeHitMap.h";
  std::ofstream fout(outputFileName.str());
  fout << "#ifndef LUT_BinaryTreeHitMap_H" << std::endl;
  fout << "#define LUT_BinaryTreeHitMap_H" << std::endl;
  fout << "namespace RD53BDecoding{" << std::endl;
  fout << "\tstatic constexpr uint32_t _LUT_BinaryTreeHitMap[" << nmap << "] = {" << std::endl;
  for(uint64_t imap = 0; imap < nmap; imap++){
    hitmap input;
    input.stream = imap;
    input.bitIdx = 0;
    input.length = length;		// First iteration: read 8-bit
    uint8_t root = getBitPair(input);

    uint32_t hitmap_rollBack = 0;
    uint32_t tot_rollBack = 0;
    
    uint32_t hitmap = 0;
    std::pair<uint16_t, uint16_t> result;
    
    switch(root){
    case 0x3:
      result = decodeRowMap(input);
      hitmap = (result.first & 0x00FF);
      result = decodeRowMap(input);
      if(result.first == 0){
    	hitmap_rollBack = result.second == 0 ? 0xff : result.second;
      }
      else{
    	hitmap |= (result.first << 8);
    	tot_rollBack = result.second;
      }
      break;
    case 0x2:		// Read the first row
      result = decodeRowMap(input);
      hitmap = (result.first & 0x00FF);
      tot_rollBack = result.second;
      break;
    case 0x1:
      result = decodeRowMap(input);
      hitmap |= (result.first << 8);
      tot_rollBack = result.second;
      break;
    }
    uint32_t summary = (hitmap_rollBack << 24) | (tot_rollBack << 16) | hitmap;
    fout << HEXF(8, summary) << ", ";
  }
  fout << "};" << std::endl;
  fout << "}" << std::endl;
  fout << "#endif" << std::endl;
  fout.close();
}

int main(int argc, char** argv){
  prepareRowLUT();
  prepareHMapLUT();
}
