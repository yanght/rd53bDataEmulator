### BASIC

To compile, make sure ROOT is installed, then run
```
mkdir -vp build && cd build && cmake .. && make -j
```

To generate encoded stream, under "build" folder run
```
./bin/encoder
```

Then to run decoding:
```
./bin/decoder
```

Many parameters can be adjusted. Please add "-h" option after each executable for details.

### ADVANCED

If you would like to change to plain hit map or toggle event making options. please edit the macro defined at beginning of RD53BDecoding/Rd53bDataProcessor.h.
